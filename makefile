ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $^
	
lib.o: lib.asm

dict.o: dict.asm

main.o: main.asm

program: lib.o dict.o main.o
	$(LD) -o $@ $^
	
.PHONY: clean
clean:
	rm -f *.o program
	
.PHONY: test
test: program
	python3 test.py
