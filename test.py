import subprocess

inputs = [
  "apple" + "\n",
  "table" + "\n",
  "tree" + "\n",
  "aslcmkm" + "\n",
  "a"*255 + "\n",
  "a"*256 + "\n"
]
expected_outputs = [
  "яблоко",
  "стол",
  "дерево",
  "",
  "",
  ""
]
expected_errors = [
  "",
  "",
  "",
  "There is no such word",
  "There is no such word",
  "The line is too long"
]
print("Running tests...")
passed = 0
format_string = lambda x: "<none>" if x == "" else "\"" + x + "\""
for i in range(len(inputs)):
    program = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = program.communicate(input=inputs[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    if (stdout == expected_outputs[i]) and (stderr == expected_errors[i]):
    	passed += 1
    else:
        print("Test " + str(i + 1) + ": failed")
        if (stdout != expected_outputs[i]):
            print("Expected in stdout: " + format_string(expected_outputs[i]) + ", in fact: " + format_string(stdout))
        else:
    	    print("Expected in stderr: " + format_string(expected_errors[i]) + ", in fact: " + format_string(stderr))
if passed == len(inputs):
    print("All tests are passed")
