%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define BUFFER_SIZE 256
section .rodata
  unknown_key_text: db "There is no such word", 0
  buffer_overflow_text: db "The line is too long", 0
section .text
global _start
_start:
  sub rsp, BUFFER_SIZE; выделяем место
  lea rdi, [rsp + 1]; аргумент 1 (адрес буфера)
  mov rsi, BUFFER_SIZE; аргумент 2 (размер буфера)
  call read_line
  test rax, rax
  jz .buffer_overflow
  mov rdi, rax; аргумент 1 (адрес считанной строки)
  mov rsi, pointer; аргумент 2 (начало словаря)
  call find_word
  test rax, rax
  jz .unknown_key
  lea rdi, [rax + 8 + rdx + 1]; адрес вхождения + длина указателя + длина ключа + нуль терминатор (теперь в rdi - адрес начала значения)
  call print_string
  call print_newline
  lea rsp, [rsp + BUFFER_SIZE]
  xor rdi, rdi
  call exit
 .buffer_overflow:
  mov rdi, buffer_overflow_text
  jmp .print_err
 .unknown_key:
  mov rdi, unknown_key_text
 .print_err:
  call print_error
  call print_newline
  lea rsp, [rsp + BUFFER_SIZE]
  mov rdi, 1
  call exit
