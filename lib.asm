section .text
%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define STDERR 2
%define STDOUT 1
%define STDIN 0
%define READ_SYSCALL 0
global exit, string_length, print_string, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy, read_line, print_error
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
   .loop:
    cmp byte[rdi + rax], 0
    jz .return
    inc	rax
    jmp .loop
   .return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov	rdx, rax
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    pop rsi
    syscall
    ret
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, WRITE_SYSCALL
    mov rax, STDOUT
    mov rsi, rsp
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10
    xor rdx, rdx
    mov rcx, rsp
    sub	rsp, 32
    dec	rcx
    mov	byte[rcx], 0
   .loop:
    div	r10
    dec	rcx
    add	dl, '0' 
    mov byte[rcx], dl
    xor	rdx, rdx
    test rax, rax
    jnz .loop
    mov	rdi, rcx
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r11, -1
    mov rax, rdi
    imul r11
    test rax, rax
    jle .was_pos
   .was_neg:
    mov	rdi, '-'
    push rax
    call print_char
    pop	rdi
;    jmp print_uint
   .was_pos:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
   .loop:
    mov	r9b, byte[rsi + rcx]
    cmp	byte[rdi + rcx], r9b
    jnz .fail
    cmp	byte[rdi + rcx], 0
    jz .return
    inc	rcx
    jmp	.loop
   .return:
    mov	rax, 1
    ret
   .fail:
    xor	rax, rax
    ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока. В случае ошибки возвращается -1.
read_char:
    push 0
    mov	rax, READ_SYSCALL
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    jz .error
    pop rax
    ret
   .error:
    inc rsp
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:    
    xor	rcx, rcx
   .loop:
    push rdi
    push rsi
    push rcx
    call read_char
    pop	rcx
    pop rsi
    pop	rdi
    cmp	al, 0x20
    jz .check
    cmp	al, 0x9
    jz .check
    cmp	al, 0xA
    jz .check
    cmp	al, 0
    jz .return
    cmp rsi, rcx
    jz .fail
    mov [rdi + rcx], rax
    inc rcx
    jmp .loop
   .check:
    test rsi, rsi
    jz .return
    test rcx, rcx
    jz .loop
    jmp	.return
   .return:
    mov	rdx, rcx
    mov	rax, rdi
    ret		
   .fail:
    xor	rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor	rax, rax
    xor r8, r8
    mov r10, 10
    xor r9, r9
   .check:
    cmp byte[rdi + r8], '0'
    jb .return
    cmp	byte[rdi + r8], '9'
    ja .return
    mul r10
    mov r9b, byte[rdi + r8]
    sub r9b, '0'
    add	rax, r9
    inc	r8
    jmp	.check
   .return:
    mov	rdx, r8
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov	r11, -1
    xor	rsi, rsi
    cmp byte[rdi], '-'
    jz .is_neg
    cmp	byte[rdi], '+'
    jz .has_plus
   .is_pos:
    call parse_uint 
    cmp	sil, '+'
    jz .add_plus
    ret
   .add_plus:
    inc	rdx
    ret
   .has_plus:
    mov rsi, rdi
    inc rdi
    jmp .is_pos
   .is_neg:
    inc rdi
    call parse_uint
    inc rdx
    push rdx
    imul r11
    pop	rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor	rax, rax
   .loop:
    cmp	rax, rdx
    jz .fail
    mov r8b, byte[rdi + rax]
    mov [rsi + rax], r8b
    cmp byte[rdi + rax], 0
    jz .return
    inc rax
    jmp .loop
   .return:
    ret
   .fail:
    xor	rax, rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Дописывает нуль терминатор
read_line:
    xor	rcx, rcx
    dec rsi; размер буфера без нуль терминатора
   .loop:
    push rdi; адрес начала
    push rsi; размер буфера
    push rcx; счетчик символов
    call read_char
    pop	rcx
    pop rsi
    pop	rdi
    cmp	rax, 0x20
    jz .check
    cmp	rax, 0x9
    jz .check
    cmp	rax, 0xA
    jz .check
   .continue:
    cmp rsi, rcx; смотрим не переполнили ли буфер
    jz .fail
    mov [rdi + rcx], rax; записываем символ в буфер
    inc rcx
    jmp .loop
   .check:
    test rcx, rcx; если самое начало строки, то пропускаем символ
    jz .loop
    cmp rax, 0xA; если перенос строки и НЕ в начале, заканчиваем читать
    jnz .continue
   .return:
    mov byte[rdi + rcx + 1], 0; дописываем нуль-терминатор
    mov	rdx, rcx
    mov	rax, rdi
    ret
   .fail:
    xor	rax, rax
    ret

; Выводит строку в stderr
print_error:
    push rdi
    call string_length
    mov	rdx, rax
    mov rax, WRITE_SYSCALL
    mov rdi, STDERR
    pop rsi
    syscall
    ret
