%include "lib.inc"
section .text
global find_word

;Принимает два аргумента: указатель на нуль-терминированную строку (rdi) и указатель на начало словаря (rsi).
;пройдёт по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено,
;вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.  
find_word:
 .loop:
  test rsi, rsi; проверяем не равен ли указатель 0
  jz .notfind
  push rsi
  lea rsi, [rsi + 8]; смещаем указатель на ключ
  call string_equals
  pop rsi
  test rax, rax
  jnz .find
  mov rsi, [rsi]; записываем в rsi указатель на следующий элемент
  jmp .loop
 .find:
  mov rax, rsi
  ret
 .notfind:
  xor rax, rax
  ret
